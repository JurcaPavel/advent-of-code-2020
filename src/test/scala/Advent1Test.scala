import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class Advent1Test extends AnyFlatSpec with Matchers {

  "Advent1" should "get input list of integers from resource" in {
    Advent1.getInputListFromResource("test1.txt") should be (List(1721, 979, 366, 299, 675, 1456))
  }

  "Advent1" should "get correct result number from list" in {
    Advent1.getResultNumberWithForComprehension(List(1721, 979, 366, 299, 675, 1456)) should be (241861950)
    Advent1.getResultNumberWithFlatMapping(List(1721, 979, 366, 299, 675, 1456)) should be (241861950)
  }
}
