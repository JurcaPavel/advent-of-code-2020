import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.io.Source

class Advent2test extends AnyFlatSpec with Matchers {
  val input: List[String] = Source.fromResource("test2.txt").getLines().toList

  "Advent2" should "return true if password on line is valid" in {
    Advent2.hasValidPassword(input.head) should be (true)
    Advent2.hasValidPassword(input(1)) should be (false)
    Advent2.hasValidPassword(input(2)) should be (false)
  }
}
