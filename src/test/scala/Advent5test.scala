import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class Advent5test extends AnyFlatSpec with Matchers {
  val testRowRange: Range = 0 to 127
  val testColumnRange: Range = 0 to 7

  "Advent5" should "get id for seat" in {
    Advent5.getIdForSeat(1, 1) should be (9)
    Advent5.getIdForSeat(65, 43) should be (563)
  }

  it should "get lower half of range" in {
    Advent5.takeLowerHalf(testRowRange) should be (0 to 63)
    Advent5.takeLowerHalf(testColumnRange) should be (0 to 3)
  }

  it should "get upper half of range" in {
    Advent5.takeUpperHalf(testRowRange) should be (64 to 127)
    Advent5.takeUpperHalf(testColumnRange) should be (4 to 7)
  }

  it should "get correct number after executing commands" in {
    Advent5.performCommands(testRowRange, "L") should be (0)
    Advent5.performCommands(testRowRange, "R") should be (64)
    Advent5.performCommands(testRowRange, "RRRRRRR") should be (127)
    Advent5.performCommands(testRowRange, "RRRRRRR") should be (127)
    Advent5.performCommands(testColumnRange, "RLR") should be (5)
  }

  it should "rule if seat id is between two occupied seat ids " in {
    Advent5.isSeatIdBetweenOccupied(7, List(6, 8)) should be (true)
    Advent5.isSeatIdBetweenOccupied(7, List(5, 6, 8, 9)) should be (true)
    Advent5.isSeatIdBetweenOccupied(2, List(5, 6, 7, 8)) should be (false)
  }
}
