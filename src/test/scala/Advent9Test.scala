import Advent9.{getInputListFromResource, solvePart1}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class Advent9Test extends AnyFlatSpec with Matchers {
  val part1Result = 127
  val part2Result = 62
  val lengthOfPreamble = 5

  "Advent9" should "get input list of integers from resource" in {
    Advent9.getInputListFromResource("test9.txt")should be (List(35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576))
  }

  it should "get preamble list" in {
    val inputList = Advent9.getInputListFromResource("test9.txt")
    Advent9.getPreamble(inputList, lengthOfPreamble) should be (List(35, 20, 15, 25, 47))
  }

  it should "tell if number is sum of two different numbers in the preamble" in {
    val preamble: List[BigInt] = List(35, 20, 15, 25, 47)
    val numberTrue = 35
    val numberFalse = 335
    Advent9.isNumberSumOfTwoNumbersInThePreamble(numberTrue, preamble) should be (true)
    Advent9.isNumberSumOfTwoNumbersInThePreamble(numberFalse, preamble) should be (false)
  }

  it should "add min and max number of set" in {
    val input: Set[BigInt] = Set(10, 20, 30)
    Advent9.addMinMax(input) should be (40)
  }

  it should "get sequence of numbers from list which sum up to the number" in {
    val input: List[BigInt] =  getInputListFromResource("test9.txt")
    val number: BigInt = Advent9.solvePart1(input, lengthOfPreamble)

    Advent9.getNumbersWhichSumUpToTheNumber(input, number, 1) should be (Set())
    Advent9.getNumbersWhichSumUpToTheNumber(input, number, 2) should be (Set())
    Advent9.getNumbersWhichSumUpToTheNumber(input, number, 3) should be (Set())
    Advent9.getNumbersWhichSumUpToTheNumber(input, number, 4) should be (Set(15, 25, 47, 40))
  }

  it should "get result number of part 1" in {
    val input: List[BigInt] =  getInputListFromResource("test9.txt")
    Advent9.solvePart1(input, lengthOfPreamble) should be (part1Result)
  }

  it should "get result number of part 2" in {
    val input: List[BigInt] =  getInputListFromResource("test9.txt")
    Advent9.solvePart2(input, solvePart1(input, lengthOfPreamble), 2) should be (part2Result)
  }
}
