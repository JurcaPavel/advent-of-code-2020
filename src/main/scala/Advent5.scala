import scala.io.Source

object Advent5 extends App {
  /*
   https://adventofcode.com/2020/day/5
   */
  val inputList: List[String] = Source.fromResource("advent5input.txt").getLines().toList
  val planeRows: Range = 0 to 127
  val planeColumns: Range = 0 to 7
  // part1
  val highestSeatId: Int = getHighestSeatId(inputList)
  println(s"Highest seatID is: $highestSeatId")
  // part2
  val mySeatId = getMySeatId(inputList)
  println(s"ID of my seat is: $mySeatId")

  def getHighestSeatId(input: List[String]): Int = {
    input.map(line => getIdFromLine(line)).max
  }

  def getMySeatId(input: List[String]): Int = {
    val occupiedSeats: List[(Int, Int)] = input.map(line => getSeatTuple(line))
    val emptySeats = for {
      x <- planeRows
      y <- planeColumns
      if !occupiedSeats.contains((x, y))
    } yield (x, y)

    val emptySeatIds = emptySeats.map(emptySeat => getIdForSeat(emptySeat._1, emptySeat._2))
    val occupiedSeatIds = occupiedSeats.map(occupiedSeat => getIdForSeat(occupiedSeat._1, occupiedSeat._2))
    val result = emptySeatIds.filter(seatId => isSeatIdBetweenOccupied(seatId, occupiedSeatIds))
    result.head
  }

  def isSeatIdBetweenOccupied(seatId: Int, occupiedSeatIds: List[Int]) = {
    occupiedSeatIds.contains(seatId + 1) && occupiedSeatIds.contains(seatId - 1)
  }

  def getSeatTuple(line: String): (Int, Int) = {
    (getRow(line take 7), getColumn(line takeRight 3))
  }

  def getIdFromLine(line: String): Int = {
    val row: Int = getRow(line take 7)
    val column: Int = getColumn(line takeRight 3)
    getIdForSeat(row, column)
  }

  def getIdForSeat(row: Int, column: Int): Int = (row * 8) + column
  def takeLowerHalf(range: Range): Range = range.dropRight(range.size / 2)
  def takeUpperHalf(range: Range): Range = range.drop(range.size / 2)
  def getRow(rowCommands: String): Int = performCommands(planeRows, rowCommands)
  def getColumn(columnCommands: String): Int = performCommands(planeColumns, columnCommands)

  def performCommands(initialRange: Range, commands: String): Int = {
    var range = initialRange
    for (command <- commands) {
      command match {
        case 'F' | 'L' => range = takeLowerHalf(range)
        case 'B' | 'R' => range = takeUpperHalf(range)
        case _ => throw new IllegalArgumentException
      }
    }
    range.head
  }
}
