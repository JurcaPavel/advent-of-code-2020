import scala.io.Source

object Advent1 extends App {
  /*
   https://adventofcode.com/2020/day/1
   find 3 numbers from the text file, which sum is 2020 -> multiply those numbers for result
   */
  val resourceName = "advent1input.txt"
  val inputList: List[Int] = getInputListFromResource(resourceName)

  val result: Int = getResultNumberWithForComprehension(inputList)
  println(result)

  val stuff: Int  = getResultNumberWithFlatMapping(inputList)
  println(stuff)

  def getInputListFromResource(resourceName: String): List[Int] = {
    Source.fromResource(resourceName).getLines()
      .map(_.toInt)
      .toList
  }

  def getResultNumberWithForComprehension(list: List[Int]): Int = {
    val l = for {
      x <- list
      y <- list
      z <- list
      if x + y + z == 2020
    } yield x * y * z
    l.toSet.head
  }

  def getResultNumberWithFlatMapping(list: List[Int]): Int = {
    list
      .flatMap(x => list
        .flatMap(y => list
          .filter(z => x + y + z == 2020)))
      .toSet
      .product
  }
}
