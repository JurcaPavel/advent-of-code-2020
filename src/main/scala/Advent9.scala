import scala.annotation.tailrec
import scala.io.Source

object Advent9 extends App {
  /*
   https://adventofcode.com/2020/day/9
   */
  val resourceName = "advent9input.txt"
  val lengthOfPreamble = 25
  val startQty = 2
  val inputList: List[BigInt] = getInputListFromResource(resourceName)

  // part 1
  val resultP1 = solvePart1(inputList, lengthOfPreamble)
  println(s"Result of part one: $resultP1")

  //part 2
  val resultP2: BigInt = solvePart2(inputList, resultP1, startQty)
  print(s"Result of part two is: $resultP2")

  def solvePart2(inputList: List[BigInt], number: BigInt, startQty: Int): BigInt = {
    @tailrec
    def impl(implInput: List[BigInt], qty: Int, result: Set[BigInt]): BigInt = {
      if (qty < implInput.size && result.sum != number) {
        val subResult = getNumbersWhichSumUpToTheNumber(implInput, number, qty)
        if (subResult.nonEmpty) addMinMax(subResult) else impl(inputList, qty + 1, subResult)
      } else 0
    }

    impl(inputList, startQty, Set())
  }

  def getNumbersWhichSumUpToTheNumber(input: List[BigInt], number: BigInt, qty: Int): Set[BigInt] = {
    @tailrec
    def impl(implInput: List[BigInt]): Set[BigInt] = {
      if (implInput.size > qty) {
        if (implInput.take(qty).sum == number) {
          implInput.take(qty).toSet
        } else {
          impl(implInput.drop(1))
        }
      } else {
        Set()
      }
    }

    if (qty == 1) Set() else impl(input)
  }


  def solvePart1(input: List[BigInt], lengthOfPreamble: Int): BigInt = {
    @tailrec
    def impl(implInput: List[BigInt]): BigInt = {
      val preamble: List[BigInt] = getPreamble(implInput, lengthOfPreamble)
      if (isNumberSumOfTwoNumbersInThePreamble(implInput(lengthOfPreamble), preamble)) {
        impl(implInput.drop(1))
      } else {
        implInput(lengthOfPreamble)
      }
    }

    impl(input)
  }

  def addMinMax(set: Set[BigInt]): BigInt = set.min + set.max
  def getPreamble(inputList: List[BigInt], length: Int): List[BigInt] = inputList.take(length)
  def isNumberSumOfTwoNumbersInThePreamble(number: BigInt, preamble: List[BigInt]): Boolean =
    preamble.flatMap(x => preamble.filter(y => x + y == number)).nonEmpty

  def getInputListFromResource(resourceName: String): List[BigInt] = {
    Source.fromResource(resourceName).getLines()
      .map(BigInt(_))
      .toList
  }
}
