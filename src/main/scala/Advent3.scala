import scala.io.Source

object Advent3 extends App {
  /*
   https://adventofcode.com/2020/day/3
   Determine the number of trees you would encounter if, for each of the following slopes,
   you start at the top-left corner and traverse the map all the way to the bottom:
   Right 1, down 1.
   Right 3, down 1. (This is the slope you already checked.)
   Right 5, down 1.
   Right 7, down 1.
   Right 1, down 2.
   What do you get if you multiply together the number of trees encountered on each of the listed slopes?
   */
  val inputList: List[String] = Source.fromResource("advent3input.txt").getLines().toList
  val horizontalLengthOfForestLine: Int = inputList.head.length
  val verticalLengthOfForest: Int = inputList.length

  val treesHit1 = moveToboggan(1, 1, 0, 0, 0)
  val treesHit2 = moveToboggan(3, 1, 0, 0, 0)
  val treesHit3 = moveToboggan(5, 1, 0, 0, 0)
  val treesHit4 = moveToboggan(7, 1, 0, 0, 0)
  val treesHit5 = moveToboggan(1, 2, 0, 0, 0)
  println(s"MULTIPLICATION OF ENCOUNTERED TREES: ${treesHit1 * treesHit2 * treesHit3 * treesHit4 * treesHit5}")

  private def moveToboggan(slopeRight: Int, slopeDown: Int, initialTreesHit: Int, initialXPosition: Int, initialYPosition: Int):BigInt = {
    def impl(slopeRight: Int, slopeDown: Int, treesHit: Int, initialXPosition: Int, initialYPosition: Int): BigInt = {
      val xPosition: Int = makeHorizontalMove(initialXPosition, slopeRight)
      val yPosition: Int = makeVerticalMove(initialYPosition, slopeDown)

      if (isInForest(yPosition)) {
        val currentPositionChar: Char = inputList(yPosition).charAt(xPosition)
        moveToboggan(slopeRight, slopeDown, if (isTree(currentPositionChar)) treesHit + 1 else treesHit, xPosition, yPosition)
      } else {
        println(s"You are over bottom! You hit: $treesHit trees!")
        treesHit
      }
    }

    impl(slopeRight, slopeDown, initialTreesHit, initialXPosition, initialYPosition)
  }

  private def makeHorizontalMove(initialXPosition: Int, amount: Int): Int = {
    (initialXPosition + amount) % horizontalLengthOfForestLine
  }

  private def makeVerticalMove(initialYPosition: Int, amount: Int): Int = initialYPosition + amount

  private def isInForest(yPosition: Int): Boolean = yPosition < verticalLengthOfForest

  private def isTree(point: Char): Boolean = point == '#'
}
