import scala.io.Source
import scala.util.matching.Regex

object Advent2 extends App {
  /*
   https://adventofcode.com/2020/day/2
   1-3 a: abcde is valid: position 1 contains a and position 3 does not.
   1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
   2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.
   */
  val inputList: List[String] = Source.fromResource("advent2input.txt").getLines().toList

  val numberOfValidPasswords = inputList.count(line => hasValidPassword(line))
  println(numberOfValidPasswords)

  def hasValidPassword(line: String): Boolean = {
    val lineRegex: Regex = new Regex("(\\d*)-(\\d*) ([a-z]): (\\w*)", "pos1", "pos2", "char", "password")
    val regexMatch: Regex.Match = lineRegex.findFirstMatchIn(line).get

    val password: String = regexMatch.group("password")
    val policyChar: Char = regexMatch.group("char").head

    val policyCharPosition1 = regexMatch.group("pos1").toInt - 1
    val policyCharPosition2 = regexMatch.group("pos2").toInt - 1

    password.charAt(policyCharPosition1) == policyChar ^ password.charAt(policyCharPosition2) == policyChar
  }
}
