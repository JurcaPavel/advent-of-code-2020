import scala.io.Source

object Advent4 extends App {
  /*
   https://adventofcode.com/2020/day/4
   byr (Birth Year) - four digits; at least 1920 and at most 2002.
   iyr (Issue Year) - four digits; at least 2010 and at most 2020.
   eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
   hgt (Height) - a number followed by either cm or in:
   If cm, the number must be at least 150 and at most 193.
   If in, the number must be at least 59 and at most 76.
   hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
   ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
   pid (Passport ID) - a nine-digit number, including leading zeroes.
   cid (Country ID) - ignored, missing or not.
   How many passports are valid?
   */
  val inputAsString = getInputStringFromResource("advent4input.txt")
  val passports: Array[String] = inputAsString.split("\\*")
  val requiredFields = Set("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")
  val heightRule = "[1]{1}[5-8]{1}[0-9]{1}cm|[1]{1}[9]{1}[0-3]{1}cm|[5]{1}[9]{1}in|[6]{1}[0-9]{1}in|[7]{1}[0-6]{1}in"
  val hairColorRule = "#[\\da-f]{6}"
  val eyeColorRule = "amb|blu|brn|gry|grn|hzl|oth"
  val ppIdRule = "[0-9]{9}"

  val numberOfValidPassports: Int = passports.count(isPPValid)
  println(numberOfValidPassports)

  // hehe PP
  private def isPPValid(ppInOneLine: String): Boolean = {
    val fieldMap: Map[String, String] =
      ppInOneLine.split(" ")
        .map(_.split(":"))
        .map(keyValueList => keyValueList.head -> keyValueList.last)
        .toMap

    val fieldNames: Set[String] = fieldMap.keys.toSet

    if (areAllRequiredFieldsPresent(fieldNames, requiredFields)) {
      areAllFieldsValid(fieldMap)
    } else {
      false
    }
  }

  private def areAllFieldsValid(fieldMap: Map[String, String]): Boolean = {
    checkBirthYear(fieldMap("byr").toInt) &&
      checkIssueYear(fieldMap("iyr").toInt) &&
      checkExpirationYear(fieldMap("eyr").toInt) &&
      checkHeight(fieldMap("hgt")) &&
      checkHairColor(fieldMap("hcl")) &&
      checkEyeColor(fieldMap("ecl")) &&
      checkPPId(fieldMap("pid"))
  }

  private def checkBirthYear(birthYear: Int): Boolean = 1920 to 2002 contains birthYear
  private def checkIssueYear(issueYear: Int): Boolean = 2010 to 2020 contains issueYear
  private def checkExpirationYear(expirationYear: Int): Boolean = 2020 to 2030 contains expirationYear
  private def checkHeight(height: String): Boolean = height.matches(heightRule)
  private def checkHairColor(hairColor: String): Boolean = hairColor.matches(hairColorRule)
  private def checkEyeColor(eyeColor: String): Boolean = eyeColor.matches(eyeColorRule)
  private def checkPPId(ppId: String): Boolean = ppId.matches(ppIdRule)
  private def areAllRequiredFieldsPresent(present: Set[String], required: Set[String]): Boolean = required.subsetOf(present)
  private def getInputStringFromResource(fileName: String): String = {
    Source.fromResource(fileName).getLines().map(line => {
      if (line.isEmpty) "*"
      else line + " "
    }).mkString
  }
}
