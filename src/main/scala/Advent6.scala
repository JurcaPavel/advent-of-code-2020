import scala.io.Source

object Advent6 extends App {
  val filename = "advent6input.txt"
  val alphabet: String = "abcdefghijklmnopqrstuvwxyz"
  val groupSeparator: Char = '*'
  val personSeparator: Char = ' '

  case class Group(persons: List[Person])
  case class Person(yesAnswers: String)

  val listOfGroups: List[Group] = getGroupsFromFile(filename)

  // part 1
  val anyoneAnsweredCount = listOfGroups
    .map(group => countAnyoneAnsweredYes(group))
    .sum
  println(s"Sum of all questions anyone in group answered yes to: $anyoneAnsweredCount")
  // part 2
  val everyoneAnsweredCount = listOfGroups
    .map(group => countEveryoneAnsweredYes(group))
    .sum
  println(s"Sum of all groups when everyone in group answered yes to question: $everyoneAnsweredCount")

  private def countAnyoneAnsweredYes(group: Group): Int = {
    alphabet.count(char => group.persons.exists(person => person.yesAnswers.contains(char)))
  }

  private def countEveryoneAnsweredYes(group: Group): Int = {
    alphabet.count(char => group.persons.forall(person => person.yesAnswers.contains(char)))
  }

  private def getGroupsFromFile(filename: String): List[Group] = {
    val separatedGroupsString = parseInputFileToString(filename)
    getGroupsFromParsedString(separatedGroupsString)
  }

  private def parseInputFileToString(filename: String) = {
    Source.fromResource(filename).getLines()
      .map(line => {
        if (line.isEmpty) groupSeparator
        else line + personSeparator
      })
      .mkString
  }

  private def getGroupsFromParsedString(parsedString: String): List[Group] = {
    parsedString.split(groupSeparator)
      .map(group =>
        Group(group.split(personSeparator)
          .map(s => Person(s))
          .toList))
      .toList
  }
}
